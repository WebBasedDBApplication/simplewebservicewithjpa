package de.loerrach.dhbw.service;

import de.loerrach.dhbw.entities.Cat;
import de.loerrach.dhbw.repos.AnimalRepository;

import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

/*
 * Kind of middleware between our REST-Controller and repository. We encapsulate repository access to be able to
 * handle various exception that might occure. We don't want to deliver HTTP-status 500 and stacktraces to our clients.
 */
public class AnimalService {

    /*
     * System.out.println(); is not the way to log sth. better use a Logger!
     * We can define eg. well formatted logs and adjust logging level (eg. info, war, error) depending on what we want to log.
     */
    private final Logger LOGGER = Logger.getLogger(AnimalService.class.getName());

    /*
     * We want CDI (TomcatEE) to take care about object instantiation
     */
    @Inject
    private AnimalRepository animalRepository;


    /*
     * For all methods here we are accessing our DB via repository, we catch all eventual exceptions that might occur
     */
    public Boolean saveCat(Cat cat){
        try{
            animalRepository.persistCat(cat);
            return true;
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return false;
        }
    }

    public Cat findByID(Long id){
        try{
            return animalRepository.findCatByID(id);
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return null;
        }
    }

    public List<Cat> getAllAnimals(){
        try{
            return animalRepository.findAll();
        }catch (Exception e){
            LOGGER.severe(e.getMessage());
            return null;
        }
    }
}
