package de.loerrach.dhbw.controller;

import de.loerrach.dhbw.entities.Cat;
import de.loerrach.dhbw.service.AnimalService;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.logging.Logger;

@Path("/api") // We define a Basepath
@Produces(MediaType.APPLICATION_JSON) // We can define a global MediaType or per Method with @Produces
public class Controller {

    // creating a Logger is always a good thing
    private static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    @Inject
    private AnimalService animalService;

    /*
     *    Simple GET-Method returning a String
     *    This is no valid JSON so we have to set other Mediatype in @Produces
     */
    @GET
    @Path("/hello")
    @Produces(MediaType.TEXT_PLAIN)
    public String getHelloWorld(){
        return "Hello World";
    }

    /*
     *    GET-Method with Path params URL: http://localhost:8080/api/cat/<a-Name>
     */
    @GET
    @Path("/cat/{name}")
    public Cat getCat(@PathParam("name") String name){
        Cat cat = new Cat(name,"Black");
        return cat;
    }

    /*
     *    GET-Method with Queryparams URL: http://localhost:8080/api/cat?name=<a-Name>
     *    If no name is passed we can provide a default one with Annotation @DefaultValue("DefaultName")
     */
    @GET
    @Path("/cat")
    public Cat getCatQuery(@QueryParam("name") @DefaultValue("Betti") String name){
        Cat cat = new Cat(name,"Black");
        return cat;
    }

    /*
     *    POST-Method URL: http://localhost:8080/api/cat
     *    We are accepting JSON as in the @Consumes Annotation defined. The JSON Body is mapped to Cat Object.
     *    @Valid ensures that all properties in Cat that are marked with @NotNullable are not null
     *    Since save-method in our service returns true or false, we'll send an appropriate response code to our clients
     */
    @POST
    @Path("/cat")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response postCat(@Valid Cat cat){
        LOGGER.info("posted cat "+cat.getName()+" and color: "+cat.getColor());
        Boolean success = animalService.saveCat(cat);
        Response.Status responseStatus = success ? Response.Status.OK : Response.Status.CONFLICT;
        return Response.status(responseStatus).build();
    }

    @GET
    @Path("/cat/all")
    public List<Cat> getAllCats(){
        return animalService.getAllAnimals();
    }
}
