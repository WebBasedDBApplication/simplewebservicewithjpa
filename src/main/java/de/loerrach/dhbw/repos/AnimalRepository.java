package de.loerrach.dhbw.repos;

import de.loerrach.dhbw.entities.Cat;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.List;

/*
 * Repositories are responsible for DB-interaction. They encapsulate all DB-access!
 */
public class AnimalRepository {

    /*
     * Our DB-access are JTA-managed, which means our runtime on TomcatEE will take care about DB-connections eg write transactions
     * in case of an error we have a build in rollback mechanism.
     */
    @PersistenceContext(unitName = "animal")
    private EntityManager entityManager;

    /*
     * Write access to DB must have the @Transactional annotation. We use the persist()-method for inserts and merge()-method for updates.
     */
   @Transactional
    public void persistCat(Cat cat){
        entityManager.persist(cat);
    }

    public Cat findCatByID(Long id){
        return entityManager.find(Cat.class,id);
    }

    /*
     * JPQL example, equivalent in SQL is "SELECT * FROM Cat" (assuming the table we query is "Cat")
     */
    public List<Cat> findAll(){
       Query query = entityManager.createQuery("SELECT c FROM Cat c");
       return query.getResultList();
    }
}
