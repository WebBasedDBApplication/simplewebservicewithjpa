package de.loerrach.dhbw.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Objects;

/*
 * Our Entity-Object has to be annotated with @Entity to be visible for JPA.
 * There is special @Table annotation where we can explicitly define which table in the DB fits to our class.
 * If no @Table annotation is provided, JPA assumes the class name eq. table name.
 */
@Entity
public class Cat{

    /*
     * Every Entity-class has to define a unique ID
     * We let JPA care about its uniqueness with the @GeneratedValue annotation
     */
    @Id
    @GeneratedValue
    private Long id;

    /*
     * All fields annotated with @NotNull have to set when sending JSON-Object to our webservice, representing a Cat instance
     */
    @NotNull
    private String name;

    @NotNull
    private String color;

    public Cat(String name, String color){
        this.name = name;
        this.color = color;
    }

    /*
     * we need an empty constructor for JSON-mapping and JPA (ORM)
     */
    protected Cat(){}

    /*
     * We can find all fields in our JSON-response having a getter-method defined!
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Long getId() {
        return id;
    }

    /*
     * It is strongly recommended to override hasCode() & equals() method of an Entity working with JPA!
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cat cat = (Cat) o;
        return Objects.equals(id, cat.id) &&
                Objects.equals(name, cat.name) &&
                Objects.equals(color, cat.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, color);
    }
}
