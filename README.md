## Sample Webservice Session 4

### Description
Rest-Service interacting with a HSQL-DB via JPA

--> There are multiple ways to implement such a REST-service there is no silver bullet

### Usage 
To test your webservice you can use a REST-Client like [Postman](https://www.getpostman.com/) or for Firefox an [REST-Client add-on](https://addons.mozilla.org/de/firefox/addon/restclient/). Remember that you might have to set HTTP-Header "Content-Type: application/json".

### Good to know
This example uses CDI (Context Dependency Injection). Per default CDI is disabled, to activate CDI in our TomcatEE, we have 
to provide a file called ``beans.xml`` under ``src/main/webapp/WEB-INF/`` it could be empty, and mostly is. 

### Further reading

* [Maven intro](https://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)
* [Maven deeper dive](https://www.vogella.com/tutorials/ApacheMaven/article.html) 
* [hashCode() and equals() for entity classes](https://javaeeblog.wordpress.com/2017/11/15/identity-and-equality-of-jpa-entities/)
* [CORS with JAX-RS](https://www.baeldung.com/cors-in-jax-rs)
* [Official TomcatEE REST-example](https://github.com/apache/tomee/tree/trunk/examples/rest-xml-json)
* [HTTP-status codes](https://wiki.selfhtml.org/wiki/HTTP/Statuscodes)